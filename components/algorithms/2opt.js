const DistanceHelper = require("../../helpers/distanceHelper");
const _ = require("lodash");
const logger = require("log4js").getLogger("[components.2opt]");

class TwoOpt {
  constructor() {}

  get IMPROVEMENTS_COUNT() {
    return 20;
  }

  run(solution) {
    return this.execute(solution);
  }

  execute(solution) {
    let improve = 0;
    const startTime = Date.now();

    while(improve < this.IMPROVEMENTS_COUNT) {
      let bestDistance = DistanceHelper.findRouteDistance(solution.route);

      solution.route.forEach((node, i) => {

        for(let k = i + 1; k < solution.route.length; k++) {
          const newRoute = this.twoOptSwap(solution.route, i, k);
          const newDistance = DistanceHelper.findRouteDistance(newRoute);

          if(newDistance < bestDistance) {
            improve = 0;
            solution.route = newRoute;
            bestDistance = newDistance;
          }
        }
      });

      improve++;
    }

    const endTime = Date.now();

    this._updateTimeSpent(endTime - startTime, solution);

    return solution;
  }

  twoOptSwap(actualRoute, i, k) {
    const newRoute = [];

    for(let c = 1; c <= i - 1; c++) {
      newRoute.push(actualRoute[c]);
    }

    const dec = 0;
    for(let c = i+1; c <= k; c++) {
      newRoute.push(actualRoute[k-dec]);
      dec++;
    }

    for(let c = k + 2; c < actualRoute.length; c++) {
      newRoute.push(actualRoute[c]);
    }

    return newRoute;
  }

  _updateTimeSpent(time, solution) {
    solution.timeSpent += time;
  }
}

module.exports = TwoOpt;