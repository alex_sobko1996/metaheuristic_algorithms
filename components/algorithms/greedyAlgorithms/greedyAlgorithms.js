const _ = require("lodash");
const DistanceHelper = require("../../../helpers/distanceHelper");

class GreedyAlgorithms {
  constructor() {}

  run(options, data) {
    return this.execute(options, data);
  }

  execute(options, data) {
    this._data = data;
    const solution = {
      distance: 0,
      timeSpent: null,
      route: []
    };

    const startTime = Date.now();

    const startPoint = this._getStartPoint(options);
    solution.route.push(startPoint);
    
    while(
      (this._data.filter(item => !item.isVisited)).length !== 0
    ) {
      const currentPoint = this._getCurrentPoint(solution);
      const distances = this._findDistances(currentPoint);
      const minDistance = this._findMinDistance(distances);
      this._enrichSolution(minDistance, solution);
    }

    const endTime = Date.now();
    solution.timeSpent = (endTime - startTime);

    return solution;
  }

  _getStartPoint(options) {
    let index;
    if(options && options.startIndex !== undefined) {
      index = options.startIndex;
    } else {
      index = Math.round(Math.random()*(this._data.length-1));
    }
    this._data[index].isVisited = true;
    return this._data[index];
  }

  _findDistances(point) {
    const filteredData = this._data.filter(item => !item.isVisited);
    return filteredData.map(item => {
      return {
        pointId: _.findIndex(this._data, item),
        distance: DistanceHelper.findDistance(point.coordinates, item.coordinates)
      };
    });
  }

  _findMinDistance(distances) {
    return _.minBy(distances, distanceObj => distanceObj.distance);
  }

  _enrichSolution(minDistance, solution) {
    this._data[minDistance.pointId].isVisited = true;
    solution.route.push(this._data[minDistance.pointId]);
    solution.distance += minDistance.distance;
  }

  _getCurrentPoint(solution) {
    const index = solution.route.length - 1;
    return solution.route[index];
  }
}

module.exports = GreedyAlgorithms;