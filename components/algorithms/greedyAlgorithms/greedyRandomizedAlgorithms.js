const GreedyAlgorithms = require("./greedyAlgorithms");
const _ = require("lodash");

class GreedyRandomizedAlgorithms extends GreedyAlgorithms {
  
  get DEFAULT_ALPHA() {
    return 0.01;
  }
  
  execute(options = {}, data) {
    this._data = data;
    const solution = {
      distance: 0,
      timeSpent: null,
      route: []
    };

    const startTime = Date.now();

    const startPoint = this._getStartPoint(options);
    solution.route.push(startPoint);
    
    while(
      (this._data.filter(item => !item.isVisited)).length !== 0
    ) {
      const currentPoint = this._getCurrentPoint(solution);
      const distances = this._findDistances(currentPoint);
      options = _.merge(options, {
        distances,
        minDistance: this._findMinDistance(distances),
        maxDistance: this._findMaxDistance(distances)
      });
      const randomCandidate = this._getRandomRestrictedCandidate(
        this._buildRestrictedCandidatesList(options)
      );
      this._enrichSolution(randomCandidate, solution);
    }

    const endTime = Date.now();
    solution.timeSpent = (endTime - startTime);

    return solution;
  }

  _buildRestrictedCandidatesList(options) {
    const filteredData = this._data.filter(item => !item.isVisited);
    const result = [];

    filteredData.forEach(item => {
      const distance = this._findCurrentDistance(options, item);
      const neighborhood = this._findNeighborhood(options);
      if(distance <= neighborhood) {
        result.push({
          pointId: _.findIndex(this._data, item),
          distance: distance
        });
      }
    });

    return result;
  }

  _getRandomRestrictedCandidate(list) {
    const index = Math.round(Math.random()*(list.length-1));
    return list[index];
  }

  _findMaxDistance(distances) {
    return _.maxBy(distances, distanceObj => distanceObj.distance);
  }

  _findCurrentDistance(options, item) {
    const index = _.findIndex(
      options.distances,
      distanceObj => distanceObj.pointId === _.findIndex(this._data, item)
    );
    return options.distances[index].distance;
  }

  _findNeighborhood(options) {
    const alpha = options.alpha || this.DEFAULT_ALPHA;
    const minDistance = options.minDistance;
    const maxDistance = options.maxDistance;

    return minDistance.distance + alpha*(maxDistance.distance - minDistance.distance);
  }
}

module.exports = GreedyRandomizedAlgorithms;