const GreedyRandomizedAlgorithms = require("../greedyAlgorithms/greedyRandomizedAlgorithms");
const _ = require("lodash");
const TwoOpt = require("../2opt");
const logger = require("log4js")
  .getLogger("[components.algorithms.graspAlgorithms.grasp]");

class GraspAlgorithms {
  constructor() {}

  run(options, data) {
    return this.execute(options, data);
  }

  execute(options, data) {
    const greedyRandomizedAlgorithms = new GreedyRandomizedAlgorithms();
    const twoOpt = new TwoOpt();
    const solution = {
      distance: +Infinity,
      timeSpent: 0,
      route: []
    };
    let i = 0;

    while(i < options.iterations) {
      const iterationSolution = greedyRandomizedAlgorithms.run(options, _.cloneDeep(data));
      if(iterationSolution.distance < solution.distance) {
        this._rebuildSolution(solution, iterationSolution);
      }

      // local search using 2opt algorithm
      solution = twoOpt.run(solution);
      this._updateTimeSpent(solution, iterationSolution);
      i++;
    }

    solution.timeSpent = `${solution.timeSpent / 1000} seconds`;
    logger.info(solution);
    
    return solution;
  }

  _rebuildSolution(solution, iterationSolution) {
    solution.distance = iterationSolution.distance;
    solution.route = iterationSolution.route;
  }

  _updateTimeSpent(solution, iterationSolution) {
    solution.timeSpent += iterationSolution.timeSpent;
  }
}

module.exports = GraspAlgorithms;