const program = require("commander");
const algorithmHelper = require("./helpers/algorithmHelper");
const logger = require("log4js").getLogger("[index]");

program
  .version("1.0.0")
  .option("-a, --algorithm [type]", "Ability to choose algorithm")
  .option("-f, --file [value]", "File name for mock data")
  .parse(process.argv);

if(program.algorithm) {
  switch(program.algorithm) {
    case "greedy":
    case "greedyRandomized":
      new algorithmHelper(program.algorithm, program.file).run("greedy");
      break;
    case "grasp":
      new algorithmHelper(program.algorithm, program.file).run("grasp");
      break;
    default:
      logger.error("No specified alrorithm");
      process.exit(0);
      break;
  }
} else {
  throw new Error("Algorithm should be passed!");
}