class AlgorithmHelper {
  constructor(algorithm, fileName) {
    this._algorithm = algorithm;
    this._fileName = fileName;
    this._options = {
      startIndex: 9,
      alpha: 0.01,
      iterations: 50
    };
  }

  run(type) {
    const algorithmExecutor = new (
      require(`../components/algorithms/${type}Algorithms/${this._algorithm}Algorithms`)
    );

    const data = require(`../mocks/${type}Algorithms/${this._algorithm}Algorithms${this._fileName || ""}`);

    algorithmExecutor.run(this._options, data);
  }
}

module.exports = AlgorithmHelper;