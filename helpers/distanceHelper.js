class DistanceHelper {
  static findDistance(first, second) {
    return Math.sqrt(
      Math.pow(second.x - first.x, 2) +
      Math.pow(second.y - first.y, 2)
    );
  }

  static findRouteDistance(route) {
    const totalDistance = 0;
    route.forEach((point, index) => {
      if(index !== route.length - 1) {
        const distance = DistanceHelper.findDistance(route[index+1], point);
        totalDistance += distance;
      }
    });
    return totalDistance;
  }
}

module.exports = DistanceHelper;