const program = require("commander");
const fs = require("fs");
const path = require("path");
const prettier = require("prettier");
const logger = require("log4js").getLogger("[scripts.parseData]");

const PRETTIER_OPTIONS = {
  singleQuote: false,
  parser: "json"
};

program
  .version("1.0.0")
  .option("-i, --inputFile [type]", "Ability to set path to parsed data")
  .option("-o, --outputFile [type]", "Ability to set the output file name")
  .parse(process.argv);

fs.readFile(path.join(__dirname, `/../data/${program.inputFile}.csv`), (err, data) => {
  if(err) {
    logger.error(err);
    process.exit(0);
  }

  let result = [];
  data = data.toString().split("\n");
  data.forEach((item, index) => {
    item = item.split(" ");
    result.push({
      name: `point_${index}`,
      coordinates: {
        x: item[1],
        y: item[2]
      }
    });
  });

  const prettyJsonView = prettier.format(JSON.stringify(result), PRETTIER_OPTIONS);
  
  fs.writeFile(
    path.join(__dirname, `/../mocks/${program.outputFile}`),
    prettyJsonView,
    err => {
      if(err) {
        logger.error(err);
        process.exit(0);
      }

      logger.info("Done");
    }
  );
});